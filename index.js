// Mock database
let posts = []

// Movie post ID


// Add post data
document.getElementById('form-add-post').addEventListener('submit', (e) => {
	// Prevents the page from loading
	e.preventDefault()
	let count = posts.length + 1
	posts.push({
		id : count,
		title : document.getElementById('txt-title').value,
		body : document.getElementById('txt-body').value
	})

	showPosts(posts)
	alert('Movie post uccessfully created.')
})

// Show posts

const showPosts = (posts) => {
	let postEntries = ''

	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">

				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
				
			</div>
		`
	})

	document.getElementById('div-post-entries').innerHTML = postEntries
}


// Edit post
const editPost = (id) => {
	let title = document.getElementById(`post-title-${id}`).innerHTML
		body = document.getElementById(`post-body-${id}`).innerHTML

	document.getElementById(`txt-edit-id`).value = id
	document.getElementById(`txt-edit-title`).value = title
	document.getElementById(`txt-edit-body`).value = body
}

// Update post
document.getElementById(`form-edit-post`).addEventListener('submit', (e) => {
	e.preventDefault()

	for(let i = 0; i < posts.length; i++) {
		if(posts[i].id === parseInt(document.getElementById(`txt-edit-id`).value)) {
			posts[i].title = document.getElementById(`txt-edit-title`).value
			posts[i].body =document.getElementById(`txt-edit-body`).value

			showPosts(posts)
			alert(`Movie post successfully updated.`)
			break
		}
	}

})

const deletePost = (id) => {
	let startingIndex = id - 1
	posts.splice(startingIndex, 1)

	showPosts(posts)
	alert(`Movie post successfully deleted.`)
}